
// Do functions depending on event
window.onscroll = function() {};
window.onload = function(){toggleLiveBar()};

// Used to toggle the menu on small screens when clicking on the menu button
function toggleNavbar() {
    var x = document.getElementById("navTablet");
    if (x.className.indexOf("w3-show") == -1) {
        x.className += " w3-show";
    } else {
        x.className = x.className.replace(" w3-show", "");
    }
}

function isLive(){
    (async () => {
        const response = await fetch('https://www.twitch.tv/pyroscythe');
        const text = await response.text();
        console.log(text.match(/(?<=\<h1>).*(?=\<\/h1>)/));
    })()

    return false
}

// Used to toggle the menu on small screens when clicking on the menu button
function toggleLiveBar() {
    var x = document.getElementById("liveStatus");
    if (x.className.indexOf("w3-show") == -1 && isLive()) {
        x.className += " w3-show";
    } else {
        x.className = x.className.replace(" w3-show", "");
    }
}
